import { Component, OnInit, NgModule } from '@angular/core';

// import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
// import { IonicModule } from '@ionic/angular';


@Component({
  selector: 'app-menunoticias',
  templateUrl: './menunoticias.component.html',
  styleUrls: ['./menunoticias.component.scss'],
})


export class MenunoticiasComponent implements OnInit {

  menuNoticias = [
    {
      imagen: '../../assets/icon/img/semana.png',
      nombre: 'Semana',
      enlace: 'https://www.semana.com'
    },
    {
      imagen: '../../assets/icon/img/el_tiempo.jpg',
      nombre: 'El Tiempo',
      enlace: 'https://www.eltiempo.com'
    },
    {
      imagen: '../../assets/icon/img/el_espectador.png',
      nombre: 'El Espectador',
      enlace: 'https://www.elespectador.com'
    },
    {
      imagen: '../../assets/icon/img/forbes.png',
      nombre: 'Forbes',
      enlace: 'https://www.forbes.com'
    }
  ];
  constructor() { }

  ngOnInit() {}

}
