import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.page.html',
  styleUrls: ['./noticias.page.scss'],
})

export class NoticiasPage {

  constructor(private menuCtrl: MenuController) { }

  irURL() {
    console.log("SI");
  }

  toggleMenuNoticias() {
    this.menuCtrl.toggle();
  }

  ngOnInit() {
  }

  menuNoticias = [
    {
      imagen: '../../assets/icon/img/semana.png',
      nombre: 'Semana',
      enlace: 'https://www.semana.com'
    },
    {
      imagen: '../../assets/icon/img/el_tiempo.jpg',
      nombre: 'El Tiempo',
      enlace: 'https://www.eltiempo.com'
    },
    {
      imagen: '../../assets/icon/img/el_espectador.png',
      nombre: 'El Espectador',
      enlace: 'https://www.elespectador.com'
    },
    {
      imagen: '../../assets/icon/img/forbes.png',
      nombre: 'Forbes',
      enlace: 'https://www.forbes.com'
    }
  ];
}
