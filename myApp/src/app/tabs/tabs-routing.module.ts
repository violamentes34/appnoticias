import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          }
        ]
      },
      {
        path: 'youtube',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../youtube/youtube.module').then(m => m.YoutubePageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/youtube',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/noticias',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
